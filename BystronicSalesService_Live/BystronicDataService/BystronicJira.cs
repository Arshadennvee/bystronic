﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using RestSharp;
//using RestSharp.Authenticators;
namespace BystronicSalesService
{
    public class BystronicJira : ServiceBase
    {
        public static string payload;
        //private static string logFilePath = "C:\\Arshad\\Bystronic\\BystronicSalesService\\BystronicDataService\\ErrorLog.txt";
        private static string logFilePath = "C:\\BystronicDataServiceUAT\\ErrorLog.txt";

        public static HttpResponseMessage CreateJira(long orderid, Order order)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            //try
            //{
            //string jiraBaseUrl = ConfigurationManager.AppSettings["jiraBaseUrl"]; // Replace with your Jira instance URL
            string username = ConfigurationManager.AppSettings["username"];
            string password = ConfigurationManager.AppSettings["password"];
            string endpoint = ConfigurationManager.AppSettings["jiraBaseUrl"]; // REST API endpoint for creating an issue
            try
            {
                // Create the JSON payload for the issue
                string issueData = @"
                        {""fields"": {
		""project"": {
			""key"": """ + ConfigurationManager.AppSettings["projectkey"] + @"""
		},
		""summary"": ""Sales Order"",
		""issuetype"": {
			""name"": ""Sales Order"",
			""id"": ""10138""
		},
		" + CreateJsonPayload(order) + @"
	}
}";
                //HttpResponseMessage response;
                // Create the HTTP client
                using (HttpClient client = new HttpClient())
                {
                    response = PostJira(username, password, endpoint, ref issueData, client);

                    // Check the response status-
                    if (response.IsSuccessStatusCode)
                    {
                        //string jsonResponse = response.Content.ReadAsStringAsync().ToString();
                        Console.WriteLine("JIRA ticket created successfully!");
                        //Console.WriteLine("Response: " + jsonResponse);
                    }
                    else
                    {

                        dynamic jsonObject = JsonConvert.DeserializeObject(issueData);
                        // Remove customfield_10328
                        ((JObject)jsonObject.fields).Remove("customfield_10328"); // Removing customer name feild from JSON
                        // Serialize the modified object back to JSON string
                        string modifiedJsonString = JsonConvert.SerializeObject(jsonObject, Formatting.Indented);
                        issueData = modifiedJsonString.ToString();
                        response = PostJira(username, password, endpoint, ref issueData, client);
                        if (response.IsSuccessStatusCode)
                        {
                            Console.WriteLine("JIRA ticket created successfully!");
                        }
                        else
                        {

                            Console.WriteLine("Error creating the JIRA ticket. Status Code: " + response.StatusCode);
                            string errorResponse = response.Content.ReadAsStringAsync().ToString();
                            Console.WriteLine("Error Response: " + errorResponse);
                            NotepadLogger.LogError($"\n\n An error occurred from add JIRA: \n Order Id : " + order.Id + "\n EquipmentNumber : " + order.EquipmentNumber + "\n Json : " + issueData + "\n Error Message : " + errorResponse + "");
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                NotepadLogger.LogError(ex.Message);
            }

            //}
            //catch (HttpRequestException ex)
            //{
            //    // Handle HTTP request related exceptions
            //    // Console.WriteLine("HTTP Request Exception: " + ex.Message);
            //    NotepadLogger.LogError($"An error occurred: {ex.Message}");
            //}
            //catch (Exception ex)
            //{
            //    // Handle other exceptions
            //    //Console.WriteLine("General Exception: " + ex.Message);
            //    NotepadLogger.LogError($"An error occurred: {ex.Message}");
            //}
            return (response);
        }

        private static HttpResponseMessage PostJira(string username, string password, string endpoint, ref string issueData, HttpClient client)
        {
            HttpResponseMessage response;
            // Set the authentication header
            string authValue = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"));
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", authValue);

            //Removing empty values
            issueData = RemoveEmptyValues(issueData);

            // Create the HTTP request content
            HttpContent content = new StringContent(issueData, Encoding.UTF8, "application/json");

            // Send the HTTP POST request
            //response = client.PostAsync(endpoint, content).Result;
            var responswait = client.PostAsync(endpoint, content);
            responswait.Wait();

            response = responswait.Result;
            return response;
        }


        private static HttpResponseMessage UpdateJiraMethod(string username, string password, string endpoint, ref string payload)
        {
            payload = RemoveEmptyValues(payload);
            HttpResponseMessage response;
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            // Encode the username and password
            string auth = Convert.ToBase64String(Encoding.ASCII.GetBytes(username + ":" + password));
            client.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Send the request to update the issue
            response = client.PutAsync(endpoint, new StringContent(payload, Encoding.UTF8, "application/json")).Result;
            return response;
        }

        public static HttpResponseMessage UpdateJira(Order order)
        {
            HttpResponseMessage response;
            string jiraBaseUrl = ConfigurationManager.AppSettings["jiraBaseUrl"]; // Replace with your Jira instance URL
            string username = ConfigurationManager.AppSettings["username"];
            string password = ConfigurationManager.AppSettings["password"]; // Use an API token instead of your actual password         
            string endpoint = $"{jiraBaseUrl}{order.ResponseJirakey}"; // REST API endpoint for creating an issue

            // Construct the payload for the update request
            string payload = @"
						 {
                           ""fields"": {" + CreateJsonPayload(order) + @"}
						 }";

            //Removing empty values
            response = UpdateJiraMethod(username, password, endpoint, ref payload);

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("JIRA ticket updated successfully.");
            }
            else
            {

                dynamic jsonObject = JsonConvert.DeserializeObject(payload);
                // Remove customfield_10328
                ((JObject)jsonObject.fields).Remove("customfield_10328"); // Removing customer name feild from JSON
                                                                          // Serialize the modified object back to JSON string
                string modifiedJsonString = JsonConvert.SerializeObject(jsonObject, Formatting.Indented);
                payload = modifiedJsonString.ToString();
                response = UpdateJiraMethod(username, password, endpoint, ref payload);
                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("JIRA ticket updated successfully.");
                }
                else
                {
                    Console.WriteLine("Failed to update JIRA ticket. Status code: " + response.StatusCode);
                    string errorMessage = response.Content.ReadAsStringAsync().ToString();
                    Console.WriteLine("Error message: " + errorMessage);
                    NotepadLogger.LogError($"\n\n An error occurred from Update JIRA: \n Order Id : " + order.Id + "\n EquipmentNumber : " + order.EquipmentNumber + "\n Json : " + payload + "\n Error Message : " + errorMessage + "");
                }
            }
            return (response);
        }

        public static string CreateJsonPayload(Order order)
        {
            try
            {
                payload = @"
		""customfield_10331"": """ + order.PONumber + @""",
		""customfield_10443"": """ + order.Comments + @""",";
                if (order.OrderEntryDate != null)
                    payload += @"""customfield_10340"": """ + Convert.ToDateTime(order.OrderEntryDate).ToString("yyyy-MM-dd HH:mm:ss") + @""",";
                if (order.MfgDate != null)
                    payload += @"""customfield_10342"": """ + Convert.ToDateTime(order.MfgDate).ToString("yyyy-MM-dd HH:mm:ss") + @""",";
                if (order.DeliveryDate != null)
                    payload += @"""customfield_10343"": """ + Convert.ToDateTime(order.DeliveryDate).ToString("yyyy-MM-dd HH:mm:ss") + @""",";
                payload += @"""customfield_10347"": {
			""self"": """ + ConfigurationManager.AppSettings["jiracustomfeildUrl"] + @"11044"",
			""value"": """ + order.MachineOrdered + @""",
			""id"": ""11044""
		},
		""customfield_10349"": """ + order.Options + @""",
		""customfield_10267"": """ + order.EquipmentNumber + @""",
		""customfield_10454"": """ + order.InstallmentNote + @""",
		""customfield_10344"": """ + order.EquipmentNumberNote + @""",
		""customfield_10464"": """ + order.StockForecastNote + @""",
		""customfield_10353"": """ + order.ScheduleNote + @""",
		""customfield_10355"": """ + order.CarrierName + @""",
		""customfield_10374"": """ + order.FreightNote + @""",";
                if (order.InstallDate != null)
                    payload += @"""customfield_10351"": """ + Convert.ToDateTime(order.InstallDate).ToString("yyyy-MM-dd HH:mm:ss") + @""",";
                payload += @"""customfield_10417"": """ + order.InstallmentTerm + @""",
		""customfield_10395"": """ + order.BacklogNote + @""",
		""customfield_10465"": """ + order.TrainingNote + @""",
		""customfield_10436"": """ + order.JobNumber + @""",";

                if (order.DateAdded != null)
                    payload += @"""customfield_10432"": """ + Convert.ToDateTime(order.DateAdded).ToString("yyyy-MM-dd") + @""",";

                payload += @"""customfield_10433"": " + order.CustomerId + @",";

                if (order.SaleAmount != null)
                    payload += @"""customfield_10435"": " + order.SaleAmount + @",";
                payload += @"""customfield_10437"": """ + order.SapNumber + @""",";

                if (order.PortDate != null)
                    payload += @"""customfield_10438"": """ + Convert.ToDateTime(order.PortDate).ToString("yyyy-MM-dd HH:mm:ss") + @""",";

                //string Shipped = order.Shipped.Replace(" ", "");
                if (!string.IsNullOrEmpty(order.Shipped))
                {
                    var tempShipped = order.Shipped.Replace(" ", "");
                    payload += @"""customfield_10468"": """ + tempShipped + @""",";
                }

                if (order.ShipDate != null)
                    payload += @"""customfield_10439"": """ + Convert.ToDateTime(order.ShipDate).ToString("yyyy-MM-dd HH:mm:ss") + @""",";

                payload += @"""customfield_10440"": """ + order.SerialNumber + @""",
		""customfield_10441"": """ + order.ProductionWeek + @""",
		""customfield_10442"": """ + order.SaleOrderNumber + @""",
		""customfield_10444"": """ + order.ForwarderNumber + @""",
		""customfield_10445"": """ + order.Port + @""",
		""customfield_10446"": """ + order.KNumber + @""",
		""customfield_10447"": """ + order.Status + @""",
        ""customfield_10328"": [{""value"": """ + order.CustomerName + @"""}],";


                if (order.FirstInstallDate != null)
                    payload += @"""customfield_10448"": """ + Convert.ToDateTime(order.FirstInstallDate).ToString("yyyy-MM-dd HH:mm:ss") + @""",";

                if (order.FinalInstallAmount != null)
                    payload += @"""customfield_10449"": " + order.FinalInstallAmount + @",";


                if (order.SecondInstallDate != null)
                    payload += @"""customfield_10450"": """ + Convert.ToDateTime(order.SecondInstallDate).ToString("yyyy-MM-dd HH:mm:ss") + @""",";

                if (order.SecondInstallAmount != null)
                    payload += @"""customfield_10451"": " + order.SecondInstallAmount + @",";
                else if (order.SecondInstallAmount != '0' && order.SecondInstallAmount != null)
                    payload += @"""customfield_10451"": " + order.SecondInstallAmount + @",";

                if (order.FinalInstallDate != null)
                    payload += @"""customfield_10452"": """ + Convert.ToDateTime(order.FinalInstallDate).ToString("yyyy-MM-dd HH:mm:ss") + @""",";

                if (order.FirstInstallAmount != null)
                    payload += @"""customfield_10453"": " + order.FirstInstallAmount + @",";

                if (order.InstallCompletedDate != null)
                    payload += @"""customfield_10456"": """ + Convert.ToDateTime(order.InstallCompletedDate).ToString("yyyy-MM-dd HH:mm:ss") + @""",";

                payload += @"""customfield_10457"": """ + order.InvoiceNumber + @""",";

                if (order.InvoiceDate != null)
                    payload += @"""customfield_10458"": """ + Convert.ToDateTime(order.InvoiceDate).ToString("yyyy-MM-dd HH:mm:ss") + @""",";

                if (order.InvoiceDate != null)
                    payload += @"""customfield_10459"": " + order.TotalInvoice + @",";

                payload += @"""customfield_10485"": """ + order.FreightIn + @""",
		""customfield_10460"": " + order.NumberOfTrucks + @",";
                if (order.Truck1 != null)
                    payload += @"""customfield_10469"": " + order.Truck1 + @",";
                if (order.Truck2 != null)
                    payload += @"""customfield_10470"": " + order.Truck2 + @",";

                if (order.Truck3 != null)
                    payload += @"""customfield_10471"": " + order.Truck3 + @",";

                if (order.Truck4 != null)
                    payload += @"""customfield_10472"": " + order.Truck4 + @",";

                if (order.Truck5 != null)
                    payload += @"""customfield_10473"": " + order.Truck5 + @",";

                if (order.Truck6 != null)
                    payload += @"""customfield_10474"": " + order.Truck6 + @",";

                if (order.Truck7 != null)
                    payload += @"""customfield_10475"": " + order.Truck7 + @",";

                if (order.Truck8 != null)
                    payload += @"""customfield_10476"": " + order.Truck8 + @",";

                if (order.Truck9 != null)
                    payload += @"""customfield_10477"": " + order.Truck9 + @",";

                if (order.Truck10 != null)
                    payload += @"""customfield_10478"": " + order.Truck10 + @",";

                if (order.Truck11 != null)
                    payload += @"""customfield_10479"": " + order.Truck11 + @",";

                if (order.Truck12 != null)
                    payload += @"""customfield_10480"": " + order.Truck12 + @",";

                if (order.Truck13 != null)
                    payload += @"""customfield_10481"": " + order.Truck13 + @",";

                if (order.Truck14 != null)
                    payload += @"""customfield_10482"": " + order.Truck14 + @",";

                if (order.Truck15 != null)
                    payload += @"""customfield_10483"": " + order.Truck15 + @",";


                payload += @"""customfield_10461"": """ + order.ToStorage + @""",";
                payload += @"""customfield_10486"": """ + order.Transload + @""",";
                if (!string.IsNullOrEmpty(order.Duty))
                    payload += @"""customfield_10487"": " + order.Duty + @",";
                else
                    payload += @"""customfield_10487"": 0,";

                if (order.MachineWarranty != null)
                    payload += @"""customfield_10462"": " + order.MachineWarranty + @",";

                if (!string.IsNullOrEmpty(order.ByCarePackage))
                    payload += @"""customfield_10463"": """ + order.ByCarePackage + @""",";

                if (order.EstimatedInstallDate != null)
                    payload += @"""customfield_10466"": """ + Convert.ToDateTime(order.EstimatedInstallDate).ToString("yyyy-MM-dd HH:mm:ss") + @""",";

                payload += @"""customfield_10484"": {
            	""self"": """ + ConfigurationManager.AppSettings["jiracustomfeildUrl"] + @"12293"",
            	""value"": """ + order.IsCanadian + @""",
            	""id"": ""12293""
            },";

                if (order.MachineCost != null)
                    payload += @"""customfield_10467"": " + order.MachineCost + @"";
            }
            catch (Exception ex)
            {
                NotepadLogger.LogError("Error from JSON creating : " + ex.Message + "");

            }
            return payload;

        }

        public static string RemoveEmptyValues(string json)
        {
            JObject jsonObject = JsonConvert.DeserializeObject<JObject>(json);
            RemoveEmptyProperties(jsonObject);
            return jsonObject.ToString();
        }

        private static void RemoveEmptyProperties(JObject jsonObject)
        {
            var propertiesToRemove = jsonObject.Properties()
                .Where(p => p.Value.Type == JTokenType.Null ||
                            (p.Value.Type == JTokenType.String && string.IsNullOrEmpty(p.Value.ToString())))
                .ToList();

            foreach (var property in propertiesToRemove)
            {
                property.Remove();
            }

            foreach (var property in jsonObject.Properties())
            {
                if (property.Value.Type == JTokenType.Undefined)
                {
                    property.Value.Replace(new JValue(""));
                }
                if (property.Value.Type == JTokenType.Object)
                {
                    RemoveEmptyProperties((JObject)property.Value);
                }
            }
        }

    }
}
