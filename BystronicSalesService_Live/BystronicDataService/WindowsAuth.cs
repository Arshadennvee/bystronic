﻿using System.Collections.Generic;
using System.Linq;
using System.DirectoryServices.AccountManagement;

namespace BystronicSalesService
{
    public class WindowsAuth
    {
        PrincipalContext AD = new PrincipalContext(ContextType.Domain);
        
        public bool IsAuthenicated(string username, string password)
        {
            return AD.ValidateCredentials(username, password);

        }

        public List<string> GetUserGroups(string username)
        {
            UserPrincipal u = UserPrincipal.FindByIdentity(AD, username);
            //var groups = from gps in u.GetAuthorizationGroups().AsQueryable() select gps.Name;

            var groups = u.GetAuthorizationGroups().Select(g => g.Name);

            return groups.ToList();
        }

        public User GetUser(string username)
        {
            var role = "";
            var userGroups = GetUserGroups(username);
            if (userGroups.Contains("CanadianUsers"))
                role = User.ROLE_CANADA;
            else 
                role = User.ROLE_US;
            UserPrincipal u = UserPrincipal.FindByIdentity(AD, username);
            return new User(username, u.Name, role, "");
        }
    }
}
