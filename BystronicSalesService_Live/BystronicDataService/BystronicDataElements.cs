﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;

namespace BystronicSalesService
{
    public class BystronicData
    {
        private static object _bystronicDataLock = new object();
        private static BystronicData _bystronicData;
        private static BystronicData _tmpBystronicData;

        public static BystronicData GetData(DataSource dataSource)
        {
            lock(_bystronicDataLock)
            {
                if(_bystronicData == null) 
                    _bystronicData = new BystronicData(dataSource);
                return _bystronicData;
            }
        }

        public static void UpdateData(DataSource dataSource)
        {
            //if (_tmpBystronicData == null)
                _tmpBystronicData = new BystronicData(dataSource);
           // else
            //    _tmpBystronicData.ReadData(dataSource);
            lock (_bystronicDataLock)
            {
                _bystronicData = _tmpBystronicData;
                //LogUtil.Trace("* Bystronic data reloaded");
            }
        }

        public List<Customer> customers;
        public List<Product> products;
        public List<ProductCategory> productCategories;
        public List<Region> regions;
        public List<RegionalManager> regionalManagers;
        public List<Payment> payments;
        public List<Order> orders;
        public List<Training> trainings;
        public List<View> views;

        public string serializedData;

        private DataSource _dataSource;

        public BystronicData(DataSource dataSource)
        {
            _dataSource = dataSource;
            ReadData(dataSource);
        }

        public void ReadData(DataSource dataSource)
        {
            try
            {
                ReadInfo(dataSource);
                ReadOrders(dataSource);
                ReadTrainings(dataSource);

                serializedData = serializeResponse(null);
            }
            catch(Exception e)
            {
                LogUtil.Trace($"Service could not load the database.\n{e.StackTrace}" );
            }
        }

        public void ReadInfo(DataSource dataSource)
        {
            customers = dataSource.GetCustomers();
            products = dataSource.GetProducts();
            productCategories = dataSource.GetProductCategories();
            regions = dataSource.GetRegions();
            regionalManagers = dataSource.GetRegionalManagers();
            payments = dataSource.GetPayments();
        }

        public void ReadOrders(DataSource dataSource)
        {
            orders = dataSource.GetOrders();
        }
        public void ReadTrainings(DataSource dataSource)
        {
            trainings = dataSource.GetTrainings();
        }

        public void AddOrder(Order order)
        {
            lock (_bystronicDataLock)
            {
                orders.Add(order);
            }
            new Thread(() => serializedData = serializeResponse(null)).Start();
        }

        public void UpdateOrder(Order order, int index)
        {
            lock (_bystronicDataLock)
            {
                orders[index] = order;
            }
            new Thread(() => serializedData = serializeResponse(null)).Start();
        }

        public void DeleteOrder(int orderId, out string equipmentNumber)
        {
            lock (_bystronicDataLock)
            {
                var order = orders.Find(o => o.Id == orderId);
                equipmentNumber = order.EquipmentNumber;
                orders.Remove(order);
            }
            new Thread(() => serializedData = serializeResponse(null)).Start();
        }

        public void AddTraining(Training training)
        {
            lock (_bystronicDataLock)
            {
                trainings.Add(training);
            }
            new Thread(() => serializedData = serializeResponse(null)).Start();
        }

        public void UpdateTraining(Training training, int index)
        {
            lock (_bystronicDataLock)
            {
                trainings[index] = training;
            }
            new Thread(() => serializedData = serializeResponse(null)).Start();
        }

        public void DeleteTraining(int orderId, out string jobNumber)
        {
            lock (_bystronicDataLock)
            {
                var training = trainings.Find(o => o.Id == orderId);
                jobNumber = training.JobNumber;
                trainings.Remove(training);
            }
            new Thread(() => serializedData = serializeResponse(null)).Start();
        }

        public void UpdateInfo()
        {
            lock (_bystronicDataLock)
            {
                ReadInfo(BystronicDataService.DataSource);
            }
            new Thread(() => serializedData = serializeResponse(null)).Start();
        }

        public Dictionary<string,object> ToDictionary(User user)
        {
            if (user != null)
            {
                orders = _dataSource.GetOrdersFor(orders, user);
            }
            return new Dictionary<string, object> { 
                { "Customers", customers }, { "Products", products }, { "ProductCategories", productCategories }, 
                { "Regions", regions }, { "RegionalManagers", regionalManagers }, { "Payments", payments }, { "Orders", orders }, { "Trainings", trainings } };
        }

        public string serializeResponse(User user)
        {
            lock (_bystronicDataLock)
            {
                var responseString = BystronicDataService.serialize(new ResponseMessage { Status = true, Query = "getdata", Data = ToDictionary(user) });
                return responseString;
            }
        }

        public ResponseMessage serializeResponseMessage(User user)
        {
            return new ResponseMessage { Status = true, Query = "getdata", Data = ToDictionary(user) };
        }
    }

    public class User
    {
        public const string ROLE_CANADA = "Canada";
        public const string ROLE_US = "US";

        public const int StartYear = 2018;

        public string ID { get; private set; }
        public string Name { get; private set; }
        public string Role { get; private set; }
        public string Region { get; private set; }
        public string CostCenter { get; private set; }
        public double YtdSale { get; private set; }

        public List<double> Sales { get; private set; } = new List<double>();

        public User(string id, string name, string role, string region, string costCenter = "", double ytdSale = 0.0, string salesGoalsString = null)
        {
            ID = id;
            Name = name;
            Role = role;
            Region = region;
            CostCenter = costCenter;
            YtdSale = ytdSale;
        }

        public User(Dictionary<string, object> data)
        {
            ID = data["ID"] as string;
            Name = data["Name"] as string;
            Role = data["Role"] as string;
            Region = data["Region"] as string;
            if (data.ContainsKey("CostCenter"))
                CostCenter = data["CostCenter"] as string;
            if (data.ContainsKey("YtdSale"))
                YtdSale = Convert.ToDouble(data["YtdSale"]);
        }
    }

    public class View
    {
        public string Name { get; private set; }
        public string Columns { get; private set; }

        public View (string name, string columns)
        {
            Name = name;
            Columns = columns;
        }
    }

    public class Permissions
    {
        public bool CanCreateOrder { get; private set; }
        public bool CanEditOrder { get; private set; }
        public bool CanViewEquipmentNumber { get; private set; }
        public bool CanViewStockForecast { get; private set; }
        public bool CanViewSchedule { get; private set; }
        public bool CanViewFreight { get; private set; }
        public bool CanViewMachinePayments { get; private set; }
        public bool CanViewBacklog { get; private set; }
        public bool CanViewTraining { get; private set; }

        public bool CanApproveOrder { get; private set; }
        public bool CanReleaseOrder { get; private set; }
        public bool CanPayOrder { get; private set; }
        public string UserGroupString { get { return string.Join(",", _userGroups);  }  }

        private List<string> _userGroups;

        public Permissions() : this (new List<string>())
        {
            CanViewEquipmentNumber = true;
            CanViewFreight = true;
            CanViewSchedule = true;
            CanViewStockForecast = true;
            CanViewMachinePayments = true;
            CanViewBacklog = true;
            CanViewTraining = true;
            CanCreateOrder = true;
            CanEditOrder = true;
        }

        public Permissions(List<string> userGroups)
        {
            _userGroups = userGroups;
             CheckPermissions();
        }

        private void CheckPermissions()
        {
            CanCreateOrder = _userGroups.Contains("SalesCreators");
            CanEditOrder = CanCreateOrder || _userGroups.Contains("SalesEditors");
            CanViewEquipmentNumber = CanCreateOrder || _userGroups.Contains("SalesEquipmentNumberViewers");
            CanViewStockForecast = CanCreateOrder || _userGroups.Contains("SalesStockForecastViewers");
            CanViewSchedule = CanCreateOrder || _userGroups.Contains("SalesScheduleViewers");
            CanViewFreight = CanCreateOrder || _userGroups.Contains("SalesFreightViewers");
            CanViewMachinePayments = CanCreateOrder || _userGroups.Contains("SalesMachinePaymentsViewers");
            CanViewBacklog = CanCreateOrder || _userGroups.Contains("SalesBacklogViewers");
            CanViewTraining = CanCreateOrder || _userGroups.Contains("SalesTrainingViewers");

            CanApproveOrder = _userGroups.Contains("Approvers");
            CanReleaseOrder = _userGroups.Contains("Releasers");
            CanPayOrder = _userGroups.Contains("Payers");

            //CanViewEquipmentNumber = true;
            //CanViewFreight = true;
            //CanViewSchedule = true;
            //CanViewStockForecast = true;
            //CanViewMachinePayments = true;
            //CanViewBacklog = true;
            //CanViewTraining = true;
            //CanCreateOrder = true;
            //CanEditOrder = true;
        }
    }

    public class Customer
    {
        public readonly int Id;
        public readonly int? RegionalManagerId;
        public readonly string Name;
        public readonly string SapNumber;
        public readonly string City;
        public readonly string State;
        public readonly string Status;
        public readonly DateTime? DateJoined;

        public Customer(int id, int? regionalManagerId, string name, string sapNumber, string city, string state, DateTime? dateJoined, string status)
        {
            Id = id;
            RegionalManagerId = regionalManagerId;
            Name = name;
            SapNumber = sapNumber;
            City = city;
            State = state;
            DateJoined = dateJoined;
            Status = status.Trim();
        }

        public Customer (Dictionary<string, object> data)
        {
            if (data.ContainsKey("Id"))
                Id = int.Parse(data["Id"].ToString());
            RegionalManagerId = (int)data["RegionalManagerId"];
            Name = data["Name"] as string;
            SapNumber = data["SapNumber"] as string;
            City = data["City"] as string;
            State = data["State"] as string;
            var dateJoinedString = data["DateJoined"] as string;
            DateTime dateJoined;
            if (dateJoinedString != null)
            {
                DateTime.TryParse(dateJoinedString, out dateJoined);
                DateJoined = dateJoined;
            }
            Status = data["Status"] as string;
        }
    }

    public class Product
    {
        public readonly int Id;
        public readonly int? CategoryId;
        public readonly string Name;
        public readonly string Status;

        public Product(int id, int? categoryId, string name, string status)
        {
            Id = id;
            CategoryId = categoryId;
            Name = name;
            Status = status.Trim();
        }

        public Product(Dictionary<string, object> data)
        {
            if (data.ContainsKey("Id"))
                Id = int.Parse(data["Id"].ToString());
            CategoryId = int.Parse(data["CategoryId"].ToString());
            Name = data["Name"] as string;
            Status = data["Status"] as string;
        }
    }

    public class ProductCategory
    {
        public readonly int Id;
        public readonly int TypeId;
        public readonly string Name;
        public readonly string Status;

        public ProductCategory(int id, int typeId, string name, string status)
        {
            Id = id;
            TypeId = typeId;
            Name = name;
            Status = status.Trim();
        }

        public ProductCategory(Dictionary<string, object> data)
        {
            if (data.ContainsKey("Id"))
                Id = int.Parse(data["Id"].ToString());
            TypeId = int.Parse(data["TypeId"].ToString());
            Name = data["Name"] as string;
            Status = data["Status"] as string;
        }
    }

    public class Region
    {
        public readonly int Id;
        public readonly string Name;

        public Region(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public Region(Dictionary<string, object> data)
        {
            if (data.ContainsKey("Id"))
                Id = int.Parse(data["Id"].ToString());
            Name = data["Name"] as string;
        }
    }

    public class RegionalManager
    {
        public readonly int Id;
        public readonly int? RegionId;
        public readonly string Name;

        public RegionalManager(int id, int? regionId, string name)
        {
            Id = id;
            RegionId = regionId;
            Name = name;
        }
        public RegionalManager(Dictionary<string, object> data)
        {
            if (data.ContainsKey("Id"))
                Id = int.Parse(data["Id"].ToString());
            Name = data["Name"] as string;
        }
    }

    public class Payment
    {
        public readonly int Id;
        public readonly int OrderId;
        public readonly DateTime? FirstInstallDate;
        public readonly decimal? FirstInstallAmount;
        public readonly DateTime? SecondInstallDate;
        public readonly decimal? SecondInstallAmount;
        public readonly DateTime? FinalInstallDate;
        public readonly decimal? FinalInstallAmount;
        public readonly string Term;
        public readonly string Note;

        public Payment(
            int id, int orderId, 
            DateTime? firstInstallDate, decimal? firstInstallAmount,
            DateTime? secondInstallDate, decimal? secondInstallAmount,
            DateTime? finalInstallDate, decimal? finalInstallAmount,
            string term, string note
        )
        {
            Id = id;
            OrderId = orderId;
            FirstInstallDate = firstInstallDate;
            FirstInstallAmount = firstInstallAmount;
            SecondInstallDate = secondInstallDate;
            SecondInstallAmount = secondInstallAmount;
            FinalInstallDate = finalInstallDate;
            FinalInstallAmount = finalInstallAmount;
            Term = term;
            Note = note;
        }

        public Payment(Dictionary<string, object> data)
        {
            DateTime date;

            if (data.ContainsKey("Id"))
                Id = int.Parse(data["Id"].ToString());
            OrderId = int.Parse(data["OrderId"].ToString());
            if (DateTime.TryParse(data["FirstInstallDate"] as string, out date)) FirstInstallDate = date;
            if (data["FirstInstallAmount"] != null)
                FirstInstallAmount = (decimal)data["FirstInstallAmount"];
            if (DateTime.TryParse(data["SecondInstallDate"] as string, out date)) SecondInstallDate = date;
            if (data["SecondInstallAmount"] != null)
                SecondInstallAmount = (decimal)data["SecondInstallAmount"];
            if (DateTime.TryParse(data["FinalInstallDate"] as string, out date)) FinalInstallDate = date;
            if (data["FinalInstallAmount"] != null)
                FinalInstallAmount = (decimal)data["FinalInstallAmount"];
            Term = data["Term"] as string;
            Note = data["Note"] as string;
        }
    }

    public class Training
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public string EquipmentNumber { get; set; }
        public string JobNumber { get; set; }
        public string CourseNumber { get; set; }
        public string CourseName { get; set; }
        public DateTime? CourseDate { get; set; }
        public int? SeatsPurchased { get; set; }
        public int? SeatsUsed { get; set; }
        public string Status { get; set; }
        public string TraineeName { get; set; }
        public string TraineePhone { get; set; }
        public string TraineeEmail { get; set; }
        public string Notes { get; set; }

        public static Training CreateTraining()
        {
            return new Training()
            {
                Id = GenerateNewID()
            };
        }

        private static int GenerateNewID()
        {
            return 0;
        }

        public Training() { }

        public Training(Dictionary<string, object> data)
        {
            DateTime date;
            if (data.ContainsKey("Id"))
                Id = int.Parse(data["Id"].ToString());
            if (data.ContainsKey("OrderId"))
                OrderId = int.Parse(data["OrderId"].ToString());
            EquipmentNumber = data["EquipmentNumber"] as string;
            JobNumber = data["JobNumber"] as string;
            CourseNumber = data["CourseNumber"] as string;
            CourseName = data["CourseName"] as string;
            if (DateTime.TryParse(data["CourseDate"] as string, out date)) CourseDate = date;

            if(data["SeatsPurchased"] != null)
                SeatsPurchased = int.Parse(data["SeatsPurchased"].ToString());
            if (data["SeatsUsed"] != null)
                SeatsUsed = int.Parse(data["SeatsUsed"].ToString());

            Status = data["Status"] as string;
            TraineeName = data["TraineeName"] as string;
            TraineePhone = data["TraineePhone"] as string;
            TraineeEmail = data["TraineeEmail"] as string;
            Notes = data["Notes"] as string;
        }
    }

    public class Order
    {
        public int Id { get; set; }
        public string EquipmentNumber { get; set; }
        public string JobNumber { get; set; }
        public string SerialNumber { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int ProductId { get; set; }
        public string SapNumber { get; set; }
        public DateTime? DateAdded { get; set; }
        public string SaleOrderNumber { get; set; }
        public string ProductName { get; set; }
        public string Category { get; set; }
        public decimal? SaleAmount { get; set; }
        public string PONumber { get; set; }
        public string Port { get; set; }
        public string ForwarderNumber { get; set; }
        public string KNumber { get; set; }
        public DateTime? PortDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public DateTime? InstallDate { get; set; }
        public DateTime? MfgDate { get; set; }
        public string Shipped { get; set; }
        public DateTime? ShipDate { get; set; }
        public string ProductionWeek { get; set; }
        public string Comments { get; set; }
        public string Options { get; set; }
        public string Status { get; set; }
        public DateTime? FirstInstallDate { get; set; }
        public decimal? FirstInstallAmount { get; set; }
        public DateTime? SecondInstallDate { get; set; }
        public decimal? SecondInstallAmount { get; set; }
        public DateTime? FinalInstallDate { get; set; }
        public decimal? FinalInstallAmount { get; set; }
        public string InstallmentTerm { get; set; }
        public string InstallmentNote { get; set; }
        public string ProjectManager { get; set; }
        public DateTime? InstallCompletedDate { get; set; }
        public DateTime? OrderEntryDate { get; set; }
        public bool? MachineOrdered { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string EntryNo { get; set; }
        public decimal? TotalInvoice { get; set; }
        public string FreightIn { get; set; }
        public int? NumberOfTrucks { get; set; }
        public decimal? Truck1 { get; set; }
        public decimal? Truck2 { get; set; }
        public decimal? Truck3 { get; set; }
        public decimal? Truck4 { get; set; }
        public decimal? Truck5 { get; set; }
        public decimal? Truck6 { get; set; }
        public decimal? Truck7 { get; set; }
        public decimal? Truck8 { get; set; }
        public decimal? Truck9 { get; set; }
        public decimal? Truck10 { get; set; }
        public decimal? Truck11 { get; set; }
        public decimal? Truck12 { get; set; }
        public decimal? Truck13 { get; set; }
        public decimal? Truck14 { get; set; }
        public decimal? Truck15 { get; set; }
        public string ToStorage { get; set; }
		public string Transload { get; set; }
        public string Duty { get; set; }
        public int? MachineWarranty { get; set; }
        public string ByCarePackage { get; set; }
        public string EquipmentNumberNote { get; set; }
        public string StockForecastNote { get; set; }
        public string ScheduleNote { get; set; }
        public string FreightNote { get; set; }
        public string BacklogNote { get; set; }
        public string TrainingNote { get; set; }
        public string CarrierName { get; set; }
        public DateTime? EstimatedInstallDate { get; set; }
        public bool? IsCanadian { get; set; }
        public decimal? MachineCost { get; set; }
        public int? ResponseJiraId { get; set; }
        public string ResponseJirakey { get; set; }
        public string ResponseJiraSelf { get; set; }

        public static Order CreateOrder()
        {
            return new Order()
            {
                Id = GenerateNewID()
            };
        }

        private static int GenerateNewID()
        {
            return 0;
        }

        public Order() { }

        public Order(Dictionary<string, object> data)
        {
            BystronicDataSource ds = new BystronicDataSource();
            DateTime date;
            if(data.ContainsKey("Id"))
                Id = int.Parse(data["Id"].ToString());
            EquipmentNumber = data["EquipmentNumber"] as string;
            JobNumber = data["JobNumber"] as string;
            SerialNumber = data["SerialNumber"] as string;
            SapNumber = data["SapNumber"] as string;
            CustomerId = (int)data["CustomerId"];
            CustomerName = ds.GetCustomers()
               .SingleOrDefault(x => x.Id == CustomerId).Name;
            ProductId = (int)data["ProductId"];
            KNumber = data["KNumber"] as string;
            if (DateTime.TryParse(data["DateAdded"] as string, out date)) DateAdded = date;
            ProductName = data["ProductName"] as string;
            EquipmentNumber = data["EquipmentNumber"] as string;
            Category = data["Category"] as string;
            SaleOrderNumber = data["SaleOrderNumber"] as string;
            if (data["SaleAmount"] != null)
                SaleAmount = decimal.Parse(data["SaleAmount"].ToString());
            PONumber = data["PONumber"] as string;
            Port = data["Port"] as string;
            ForwarderNumber = data["ForwarderNumber"] as string;
            if (DateTime.TryParse(data["MfgDate"] as string, out date)) MfgDate = date;
            if (DateTime.TryParse(data["PortDate"] as string, out date)) PortDate = date;
            if (DateTime.TryParse(data["DeliveryDate"] as string, out date)) DeliveryDate = date;
            if (DateTime.TryParse(data["InstallDate"] as string, out date)) InstallDate = date;
            Shipped = data["Shipped"] as string;
            if (DateTime.TryParse(data["ShipDate"] as string, out date)) ShipDate = date;
            ProductionWeek = data["ProductionWeek"] as string;
            Comments = data["Comments"] as string;
            Options = data["Options"] as string;
            Status = data["Status"] as string;

            if (DateTime.TryParse(data["FirstInstallDate"] as string, out date)) FirstInstallDate = date;
            if (data["FirstInstallAmount"] != null)
            {
                var value = data["FirstInstallAmount"].ToString();
                FirstInstallAmount = (decimal)float.Parse(value as string);
            }
            if (DateTime.TryParse(data["SecondInstallDate"] as string, out date)) SecondInstallDate = date;
            if (data["SecondInstallAmount"] != null)
            {
                var value = data["SecondInstallAmount"].ToString();
                SecondInstallAmount = (decimal)float.Parse(value as string);
            }
            if (DateTime.TryParse(data["FinalInstallDate"] as string, out date)) FinalInstallDate = date;
            if (data["FinalInstallAmount"] != null)
            {
                var value = data["FinalInstallAmount"].ToString();
                FinalInstallAmount = (decimal)float.Parse(value as string);
            }
            InstallmentTerm = data["InstallmentTerm"] as string;
            InstallmentNote = data["InstallmentNote"] as string;
            ProjectManager = data["ProjectManager"] as string;
            if (DateTime.TryParse(data["InstallCompletedDate"] as string, out date)) InstallCompletedDate = date;
            if (DateTime.TryParse(data["OrderEntryDate"] as string, out date)) OrderEntryDate = date;
            if (data["MachineOrdered"] != null)
            {
                var value = data["MachineOrdered"].ToString();
                MachineOrdered = bool.Parse(value as string);
            }
            InvoiceNumber = data["InvoiceNumber"] as string;
            if (DateTime.TryParse(data["InvoiceDate"] as string, out date)) InvoiceDate = date;
            EntryNo = data["EntryNo"] as string;

            if (data["TotalInvoice"] != null)
            {
                var value = data["TotalInvoice"].ToString();
                TotalInvoice = (decimal)float.Parse(value as string);
            }
            FreightIn = data["FreightIn"] as string;
            if (data["NumberOfTrucks"] != null)
            {
                var value = data["NumberOfTrucks"].ToString();
                NumberOfTrucks = int.Parse(value as string);
            }
            if (data["Truck1"] != null)
            {
                var value = data["Truck1"].ToString();
                Truck1 = (decimal)float.Parse(value as string);
            }
            if (data["Truck2"] != null)
            {
                var value = data["Truck2"].ToString();
                Truck2 = (decimal)float.Parse(value as string);
            }
            if (data["Truck3"] != null)
            {
                var value = data["Truck3"].ToString();
                Truck3 = (decimal)float.Parse(value as string);
            }
            if (data["Truck4"] != null)
            {
                var value = data["Truck4"].ToString();
                Truck4 = (decimal)float.Parse(value as string);
            }
            if (data["Truck5"] != null)
            {
                var value = data["Truck5"].ToString();
                Truck5 = (decimal)float.Parse(value as string);
            }
            if (data["Truck6"] != null)
            {
                var value = data["Truck6"].ToString();
                Truck6 = (decimal)float.Parse(value as string);
            }
            if (data["Truck7"] != null)
            {
                var value = data["Truck7"].ToString();
                Truck7 = (decimal)float.Parse(value as string);
            }
            if (data["Truck8"] != null)
            {
                var value = data["Truck8"].ToString();
                Truck8 = (decimal)float.Parse(value as string);
            }
            if (data["Truck9"] != null)
            {
                var value = data["Truck9"].ToString();
                Truck9 = (decimal)float.Parse(value as string);
            }
            if (data["Truck10"] != null)
            {
                var value = data["Truck10"].ToString();
                Truck10 = (decimal)float.Parse(value as string);
            }
            if (data["Truck11"] != null)
            {
                var value = data["Truck11"].ToString();
                Truck11 = (decimal)float.Parse(value as string);
            }
            if (data["Truck12"] != null)
            {
                var value = data["Truck12"].ToString();
                Truck12 = (decimal)float.Parse(value as string);
            }
            if (data["Truck13"] != null)
            {
                var value = data["Truck13"].ToString();
                Truck13 = (decimal)float.Parse(value as string);
            }
            if (data["Truck14"] != null)
            {
                var value = data["Truck14"].ToString();
                Truck14 = (decimal)float.Parse(value as string);
            }
            if (data["Truck15"] != null)
            {
                var value = data["Truck15"].ToString();
                Truck15 = (decimal)float.Parse(value as string);
            }

            ToStorage = data["ToStorage"] as string;
            Transload = data["Transload"] as string;
            Duty = data["Duty"] as string;

            if (data["MachineWarranty"] != null)
            {
                var value = data["MachineWarranty"].ToString();
                MachineWarranty = int.Parse(value as string);
            }

            ByCarePackage = data["ByCarePackage"] as string;

            EquipmentNumberNote = data["EquipmentNumberNote"] as string;

            StockForecastNote = data["StockForecastNote"] as string;

            ScheduleNote = data["ScheduleNote"] as string;

            FreightNote = data["FreightNote"] as string;

            BacklogNote = data["BacklogNote"] as string;

            TrainingNote = data["TrainingNote"] as string;

            CarrierName = data["CarrierName"] as string;

            if (DateTime.TryParse(data["EstimatedInstallDate"] as string, out date)) EstimatedInstallDate = date;

            if (data.ContainsKey("IsCanadian") && data["IsCanadian"] != null)
                IsCanadian = bool.Parse(data["IsCanadian"].ToString());

            if (data["MachineCost"] != null)
            {
                var value = data["MachineCost"].ToString();
                MachineCost = (decimal)float.Parse(value as string);
            }

            //ResponseJiraId =  (int)data["ResponseJiraId"];

            ResponseJiraId = 0;
            if (data["ResponseJiraId"] != null)
            {
                var value = data["ResponseJiraId"].ToString();
                ResponseJiraId = (int)float.Parse(value as string);
            }

            ResponseJirakey = data["ResponseJirakey"] as string;

            ResponseJiraSelf = data["ResponseJiraSelf"] as string;

          

        }

    public List<string> GetChangedFields(Order otherOrder)
        {
            var changedFields = new List<string>();
            if (JobNumber != otherOrder.JobNumber) changedFields.Add("JobNumber");
            if (SerialNumber != otherOrder.SerialNumber) changedFields.Add("SerialNumber");
            if (PONumber != otherOrder.PONumber) changedFields.Add("PONumber");
            if (DateAdded != otherOrder.DateAdded) changedFields.Add("DateAdded");
            if (SaleOrderNumber != otherOrder.SaleOrderNumber) changedFields.Add("SaleOrderNumber");
            if (ProductName != otherOrder.ProductName) changedFields.Add("ProductName");
            if (EquipmentNumber != otherOrder.EquipmentNumber) changedFields.Add("EquipmentNumber");
            if (Category != otherOrder.Category) changedFields.Add("Category");
            if (SaleAmount != otherOrder.SaleAmount) changedFields.Add("SaleAmount");
            if (PortDate != otherOrder.PortDate) changedFields.Add("PortDate");
            if (DeliveryDate != otherOrder.DeliveryDate) changedFields.Add("DeliveryDate");
            if (InstallDate != otherOrder.InstallDate) changedFields.Add("InstallDate");
            if (Shipped != otherOrder.Shipped) changedFields.Add("Shipped");
            if (ShipDate != otherOrder.ShipDate) changedFields.Add("ShipDate");
            if (ProductionWeek != otherOrder.ProductionWeek) changedFields.Add("ProductionWeek");
            if (Options != otherOrder.Options) changedFields.Add("Options");
            if (Status != otherOrder.Status) changedFields.Add("Status");
            if (IsCanadian != otherOrder.IsCanadian) changedFields.Add("IsCanadian");
            return changedFields;
        }
    }
}
