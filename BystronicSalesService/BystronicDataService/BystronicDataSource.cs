﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BystronicSalesService
{
    class BystronicDataSource : DataSource
    {
        BystronicDataDataContext bd = new BystronicDataDataContext();

        public override bool IsReady() => true;

        // PRODUCTS ////////////////////////////////////////////////////////////////////////////////////////////////

        public override List<Product> GetProducts() =>
           bd.ProductLists.Select(p => new Product(p.Id, p.CategoryID, p.ProductName, p.Status)).ToList();

        public override bool AddProduct(Product product)
        {
            bd.AddProduct(product.Name, product.CategoryId, product.Status);
            bd.SubmitChanges();
            return true;
        }

        public override bool UpdateProduct(Product product)
        {
            bd.UpdateProduct(product.Id, product.Name, product.CategoryId, product.Status);
            bd.SubmitChanges();
            return true;
        }

        public override bool DeleteProduct(Product product)
        {
            //delete product only if there arent orders with this product
            var result = bd.DeleteProduct(product.Id).FirstOrDefault();
            //returns true if deleted, returns false if orders with customer id already exists
            return result.Column1.Value;
        }


        // PRODUCT CATEGORIES ////////////////////////////////////////////////////////////////////////////////////////////////

        public override List<ProductCategory> GetProductCategories() =>
            bd.ProductCategoryLists.Select(c => new ProductCategory(c.Id, c.CategoryTypeId, c.Category, c.Status)).ToList();

        public override bool AddProductCategory(ProductCategory productCategory)
        {
            bd.AddProductCategory(productCategory.TypeId, productCategory.Name, productCategory.Status);
            bd.SubmitChanges();
            return true;
        }

        public override bool UpdateProductCategory(ProductCategory productCategory)
        {
            bd.UpdateProductCategory(productCategory.Id, productCategory.TypeId, productCategory.Name, productCategory.Status);
            bd.SubmitChanges();
            return true;
        }

        // CUSTOMERS ////////////////////////////////////////////////////////////////////////////////////////////////

        public override List<Customer> GetCustomers() =>
            bd.CustomerLists.Select(c => new Customer(c.Id, c.ReginalManagerID, c.Name, c.SAPNumber, c.City, c.State, c.DateJoined, c.Status)).ToList();

        public override bool AddCustomer(Customer customer)
        {
            bd.AddCustomer(customer.SapNumber, customer.Name, customer.City, customer.State, customer.RegionalManagerId, customer.DateJoined, customer.Status);
            return true;
        }

        public override bool UpdateCustomer(Customer customer)
        {
            bd.UpdateCustomer(customer.Id, customer.SapNumber, customer.Name, customer.City, customer.State, customer.RegionalManagerId, customer.DateJoined, customer.Status);
            return true;
        }

        public override bool DeleteCustomer(Customer customer)
        {
            //delete customer only if there are no orders for this customer otherwise we need to delete orders. Maybe a message to use "Please delete orders for this customer before deleting customer";
            var result = bd.DeleteCustomer(customer.Id).FirstOrDefault();
            //returns true if deleted, returns false if orders with customer id already exists
            return result.Column1.Value;
        }


        // REGIONS ////////////////////////////////////////////////////////////////////////////////////////////////

        public override List<Region> GetRegions() =>
            bd.RegionLists.Select(r => new Region(r.Id, r.RegionName)).ToList();

        public override bool AddRegion(Region region)
        {
            bd.AddRegion(region.Name);
            return true;
        }

        public override bool UpdateRegion(Region region)
        {
            bd.UpdateRegion(region.Id, region.Name);
            return true;
        }

        // REGIONAL MANAGERS ////////////////////////////////////////////////////////////////////////////////////////////////

        public override List<RegionalManager> GetRegionalManagers() =>
            bd.RegionalManagerLists.Select(r => new RegionalManager(r.Id, r.RegionID, r.RegionalManagerName)).ToList();

        public override bool AddRegionalManager(RegionalManager regionalManager)
        {
            bd.AddRegionalManager(regionalManager.Name, regionalManager.RegionId);
            return true;
        }

        public override bool UpdateRegionalManager(RegionalManager regionalManager)
        {
            bd.UpdateRegionalManager(regionalManager.Id, regionalManager.Name, regionalManager.RegionId);
            return true;
        }

        // PAYMENTS ////////////////////////////////////////////////////////////////////////////////////////////////

        public override List<Payment> GetPayments() =>
            bd.PaymentLists.Select(p => new Payment(p.Id, p.OrderID, p.FinalInstallDate, p.FirstInstallAmount, p.SecondInstallDate, p.SecondInstallAmount, p.FinalInstallDate, p.FinalInstallAmount, p.Term, p.Note)).ToList();

        public override bool AddPayment(Payment payment)
        {
            bd.AddPayment(payment.OrderId, payment.FirstInstallDate, payment.FirstInstallAmount, payment.SecondInstallDate, payment.SecondInstallAmount, payment.FinalInstallDate, payment.FinalInstallAmount, payment.Term, payment.Note);
            return true;
        }

        public override bool UpdatePayment(Payment payment)
        {
            bd.UpdatePayment(payment.Id, payment.OrderId, payment.FirstInstallDate, payment.FirstInstallAmount, payment.SecondInstallDate, payment.SecondInstallAmount, payment.FinalInstallDate, payment.FinalInstallAmount, payment.Term, payment.Note);
            return true;
        }


        public override List<Order> GetOrders()
        {
            List<Order> orders = new List<Order>();

            foreach (OrderList p in bd.OrderLists.AsEnumerable())
            {
                orders.Add(new Order()
                {
                    Id = p.Id,
                    EquipmentNumber = p.EquipmentNumber,
                    JobNumber = p.JobNumber,
                    SerialNumber = p.SerialNumber,
                    CustomerId = p.CustomerID,
                    ProductId = p.ProductID,
                    SapNumber = p.SAPNumber,
                    DateAdded = p.DateAdded,
                    SaleOrderNumber = p.SaleOrderNumber,
                    KNumber = p.KNumber,
                    SaleAmount = p.SaleAmount,
                    PONumber = p.PONumber,
                    Port = p.Port,
                    ForwarderNumber = p.ForwarderNumber,
                    PortDate = p.PortDate,
                    MfgDate = p.MfgDate,
                    DeliveryDate = p.DeliveryDate,
                    InstallDate = p.InstallDate,
                    Shipped = p.Shipped,
                    ShipDate = p.ShipDate,
                    ProductionWeek = p.ProductionWeek,
                    Comments = p.Comments,
                    Options = p.Options,
                    Status = p.Status,
                    FirstInstallDate = p.FirstInstallDate,
                    FirstInstallAmount = p.FirstInstallAmount,
                    SecondInstallDate = p.SecondInstallDate,
                    SecondInstallAmount = p.SecondInstallAmount,
                    FinalInstallDate = p.FinalInstallDate,
                    FinalInstallAmount = p.FinalInstallAmount,
                    InstallmentTerm = p.InstallmentTerm,
                    InstallmentNote = p.InstallmentNote,
                    ProjectManager = p.ProjectManager,
                    InstallCompletedDate = p.InstallCompletedDate,
                    OrderEntryDate = p.OrderEntryDate,
                    MachineOrdered = p.MachineOrdered,
                    InvoiceNumber = p.InvoiceNumber,
                    InvoiceDate = p.InvoiceDate,
                    EntryNo = p.EntryNo,
                    TotalInvoice = p.TotalInvoice,
                    FreightIn = p.FreightIn,
                    NumberOfTrucks = p.NumberOfTrucks,
                    Truck1 = p.Truck1,
                    Truck2 = p.Truck2,
                    Truck3 = p.Truck3,
                    Truck4 = p.Truck4,
                    Truck5 = p.Truck5,
                    Truck6 = p.Truck6,
                    Truck7 = p.Truck7,
                    Truck8 = p.Truck8,
                    Truck9 = p.Truck9,
                    Truck10 = p.Truck10,
                    Truck11 = p.Truck11,
                    Truck12 = p.Truck12,
                    Truck13 = p.Truck13,
                    Truck14 = p.Truck14,
                    Truck15 = p.Truck15,
                    ToStorage = p.ToStorage,
                    Transload = p.Transload,
                    Duty = p.Duty,
                    MachineWarranty = p.MachineWarranty,
                    ByCarePackage = p.ByCarePackage,
                    EquipmentNumberNote = p.EquipmentNumberNote,
                    StockForecastNote = p.StockForecastNote,
                    ScheduleNote = p.ScheduleNote,
                    FreightNote = p.FreightNote,
                    BacklogNote = p.BacklogNote,
                    TrainingNote = p.TrainingNote,
                    CarrierName = p.CarrierName,
                    EstimatedInstallDate = p.EstimatedInstallDate,
                    IsCanadian = p.IsCanadian,
                    MachineCost = p.MachineCost,
                    ResponseJiraId = p.ResponseJiraId,
                    ResponseJirakey = p.ResponseJirakey,
                    ResponseJiraSelf = p.ResponseJiraSelf

                });
            }

            return orders;
        }

        public override bool AddOrder(Order order)
        {
            //add new order: ignore order.ID
            BystronicDataDataContext bd = new BystronicDataDataContext();
            order.Id = bd.AddOrder(
                  order.EquipmentNumber
                , order.JobNumber
                , order.SerialNumber
                , order.CustomerId
                , order.ProductId
                , order.SaleAmount
                , order.SapNumber
                , order.PONumber
                , order.KNumber
                , order.PortDate
                , order.Port
                , order.ForwarderNumber
                , order.DeliveryDate
                , order.MfgDate
                , order.InstallDate
                , order.Shipped
                , order.ShipDate
                , order.ProductionWeek
                , order.SaleOrderNumber
                , order.Comments
                , order.Options
                , order.Status
                , order.FirstInstallDate
                , order.FirstInstallAmount
                , order.SecondInstallDate
                , order.SecondInstallAmount
                , order.FinalInstallDate
                , order.FinalInstallAmount
                , order.InstallmentTerm
                , order.InstallmentNote
                , order.ProjectManager
                , order.InstallCompletedDate
                , order.OrderEntryDate
                , order.MachineOrdered
                , order.InvoiceNumber
                , order.InvoiceDate
                , order.EntryNo
                , order.TotalInvoice
                , order.FreightIn
                , order.NumberOfTrucks
                , order.Truck1
                , order.Truck2
                , order.Truck3
                , order.Truck4
                , order.Truck5
                , order.Truck6
                , order.Truck7
                , order.Truck8
                , order.Truck9
                , order.Truck10
                , order.Truck11
                , order.Truck12
                , order.Truck13
                , order.Truck14
                , order.Truck15
                , order.ToStorage
                , order.Transload
                , order.Duty
                , order.MachineWarranty
                , order.ByCarePackage
                , order.EquipmentNumberNote
                , order.StockForecastNote
                , order.ScheduleNote
                , order.FreightNote
                , order.BacklogNote
                , order.TrainingNote
                , order.CarrierName
                , order.EstimatedInstallDate
                , order.IsCanadian
                , order.MachineCost
            //, order.ResponseJiraId
            // , order.ResponseJirakey
            //, order.ResponseJiraSelf
            );

            bd.SubmitChanges();

            return true;
        }

        public override bool UpdateOrder(Order order)
        {
            //update order in the database with the order.ID with given order
            bd.UpdateOrder(
                  //order.orderID  I need order id
                  order.Id
                , order.EquipmentNumber
                , order.JobNumber
                , order.SerialNumber
                , order.CustomerId
                , order.ProductId
                , order.SaleAmount
                , order.SapNumber
                , order.PONumber
                , order.KNumber
                , order.PortDate
                , order.Port
                , order.ForwarderNumber
                , order.DeliveryDate
                , order.MfgDate
                , order.InstallDate
                , order.Shipped
                , order.ShipDate
                , order.ProductionWeek
                , order.SaleOrderNumber
                , order.Comments
                , order.Options
                , order.Status
                , order.FirstInstallDate
                , order.FirstInstallAmount
                , order.SecondInstallDate
                , order.SecondInstallAmount
                , order.FinalInstallDate
                , order.FinalInstallAmount
                , order.InstallmentTerm
                , order.InstallmentNote
                , order.ProjectManager
                , order.InstallCompletedDate
                , order.OrderEntryDate
                , order.MachineOrdered
                , order.InvoiceNumber
                , order.InvoiceDate
                , order.EntryNo
                , order.TotalInvoice
                , order.FreightIn
                , order.NumberOfTrucks
                , order.Truck1
                , order.Truck2
                , order.Truck3
                , order.Truck4
                , order.Truck5
                , order.Truck6
                , order.Truck7
                , order.Truck8
                , order.Truck9
                , order.Truck10
                , order.Truck11
                , order.Truck12
                , order.Truck13
                , order.Truck14
                , order.Truck15
                , order.ToStorage
                , order.Transload
                , order.Duty
                , order.MachineWarranty
                , order.ByCarePackage
                , order.EquipmentNumberNote
                , order.StockForecastNote
                , order.ScheduleNote
                , order.FreightNote
                , order.BacklogNote
                , order.TrainingNote
                , order.CarrierName
                , order.EstimatedInstallDate
                , order.IsCanadian
                , order.MachineCost
                //, order.ResponseJiraId
                //, order.ResponseJirakey
                //, order.ResponseJiraSelf
            );

            //should we update or delete and reinsert?
            bd.SubmitChanges();
            return true;
        }

        public override bool DeleteOrder(int orderID)
        {
            bd.DeleteOrder(orderID);
            return true;
        }

        public override bool UpdateJiraResposetoDB(Order order)
        {
            //update order in the database with the order.ID with given order
            bd.UpdateJira(
                  //order.orderID  I need order id
                  order.Id,
                  order.ResponseJiraId,
                  order.ResponseJirakey,
                  order.ResponseJiraSelf
            );

            //should we update or delete and reinsert?
            bd.SubmitChanges();
            return true;
        }

        public override List<Training> GetTrainings()
        {
            List<Training> trainings = new List<Training>();

            foreach (TrainingList p in bd.TrainingLists.AsEnumerable())
            {
                trainings.Add(new Training()
                {
                    Id = p.Id,
                    OrderId = p.OrderId,
                    EquipmentNumber = p.EquipmentNumber,
                    JobNumber = p.JobNumber,
                    CourseNumber = p.CourseNumber,
                    CourseName = p.CourseName,
                    CourseDate = p.CourseDate,
                    SeatsPurchased = p.SeatsPurchased,
                    SeatsUsed = p.SeatsUsed,
                    Status = p.Status,
                    TraineeName = p.TraineeName,
                    TraineePhone = p.TraineePhone,
                    TraineeEmail = p.TraineeEmail,
                    Notes = p.Notes
                });
            }

            return trainings;
        }

        public override bool AddTraining(Training training)
        {
            //add new order: ignore order.ID
            training.Id = bd.AddTraining(
                  training.OrderId
                , training.EquipmentNumber
                , training.JobNumber
                , training.CourseNumber
                , training.CourseName
                , training.CourseDate
                , training.SeatsPurchased
                , training.SeatsUsed
                , training.Status
                , training.TraineeName
                , training.TraineePhone
                , training.TraineeEmail
                , training.Notes
            );

            bd.SubmitChanges();

            return true;
        }

        public override bool UpdateTraining(Training training)
        {
            //update order in the database with the order.ID with given order
            bd.UpdateTraining(
                  //order.orderID  I need order id
                  training.Id
                , training.OrderId
                , training.EquipmentNumber
                , training.JobNumber
                , training.CourseNumber
                , training.CourseName
                , training.CourseDate
                , training.SeatsPurchased
                , training.SeatsUsed
                , training.Status
                , training.TraineeName
                , training.TraineePhone
                , training.TraineeEmail
                , training.Notes
            );

            //should we update or delete and reinsert?
            bd.SubmitChanges();
            return true;
        }

        public override bool DeleteTraining(int trainingID)
        {
            bd.DeleteTraining(trainingID);
            return true;
        }

        // COLUMNS ///////////////////////////////////////////////////////////////////////////////

        public override string GetColumns(string username, string gridname)
        {
            GetGridcolumnsByUserResult result = bd.GetGridcolumnsByUser(username, gridname).FirstOrDefault();
            return result != null ? result.gridcolumns : string.Empty;
        }

        public override List<View> GetViews(string username)
        {
            var views = from view in bd.GetViewsByUser(username) select new View(view.gridname.Trim(), view.gridcolumns.Trim());
            return views.ToList();
        }

        public override void SaveViews(string username, List<View> views)
        {
            bd.DeleteAllGridsByUser(username);
            foreach (var view in views)
            {
                bd.AddUpdateUsersGridColumns(username, view.Name, view.Name, view.Columns);
            }
        }

        public override void SaveColumns(string username, string gridname, string newgridname, string columns) =>
            bd.AddUpdateUsersGridColumns(username, gridname, newgridname, columns);

        public override void DeleteColumns(string username, string gridname) =>
            bd.DeleteGridByUserAndGridName(username, gridname);
    }
}
