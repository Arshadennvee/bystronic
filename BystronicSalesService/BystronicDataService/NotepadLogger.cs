﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BystronicSalesService
{
    internal class NotepadLogger
    {
        //PROD
        private static string logFilePath = "C:\\BystronicDataService\\ErrorLog.txt";
        
        //UAT
        //private static string logFilePath = "C:\\BystronicDataServiceUAT\\ErrorLog.txt";

        //Local
        //private static string logFilePath = "C:\\Arshad\\Bystronic\\BystronicSalesService\\BystronicDataService\\ErrorLog.txt";

        public static void LogError(string errorMessage)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(logFilePath, true))
                {
                    writer.WriteLine($"{DateTime.Now}: {errorMessage} \n ----------------------------------------------------------------------------");
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions that may occur while logging
                Console.WriteLine($"Error while logging: {ex.Message}");
            }
        }
    }
}
