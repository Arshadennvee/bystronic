﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BystronicSalesService
{
    internal class Bystronic
    {
        public int id { get; set; }
        public string key { get; set; }
        public string self { get; set; }
        public String JiraUserName { get; set; }
        public String JiraPassword { get; set; }
        public String JiraUrl { get; set; }
        public String JiraJson { get; set; }
    }
}
