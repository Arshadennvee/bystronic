﻿function TruckGrid(scope, gridID, onTruckSelected) {
    this.scope = scope;
    this.gridID = gridID;
    this.data = [];
    var self = this;
    var currentCell;

    $(this.gridID).jqGrid({
        colNames: ['Truck', 'Amount'],
        colModel: 
            [
                { name: 'Name', width: 50, sorttype: "string", editable: false, autoResizing: false },
                {
                    name: 'Value', width: 100, editable: true, autoResizing: false, align: 'right', formatter: currencyValueFormatter, unformat: unformatCurrencyValue,
                    formatoptions: { prefix: '$', suffix: '', thousandsSeparator: ',', decimalPlaces: 2, defaultValue: '' }
                }
            ],
        data: self.data,
        datatype: "local",
        autowidth: false,
        shrinkToFit: false,
        width: 150,
        cellEdit: true,

        rowNum: 10,
        rownumbers: false,
        hoverrows: false,
        viewrecords: true,
        caption: null,
        hidegrid: false,
        multiselect: false,
        selectionmode: "singlecell",
        cmTemplate: { title: false, resizable: false },
        loadComplete: function () {
            var rowIds = jQuery(self.gridID).jqGrid('getDataIDs');
        },
        beforeSelectRow: function (rowId, e) {
            var iCol = $.jgrid.getCellIndex($(e.target).closest("td")[0]);

            return false;
        },
        beforeEditCell: function (rowId, cellname, value, iRow, iCol) {
            currentCell = {
                rowid: rowId, cellname: cellname, value: value, iRow: iRow, iCol: iCol
            }
        },
        afterSaveCell: function (rowid, cellname, value, iRow, iCol) {
            self.updateAmount(iRow, value);
            onTruckSelected(self.scope.tmpOrder);
            setTimeout(function () { jQuery(self.gridID).jqGrid("editCell", iRow < 15 ? iRow + 1 : 1, 1, true); }, 100);
        }
    });


    this.updateAmount = function (row, strValue) {
        let value = strValue ? parseFloat(strValue) : 0;
        switch (row) {
            case 1:
                self.scope.tmpOrder.Truck1 = value;
                break;
            case 2:
                self.scope.tmpOrder.Truck2 = value;
                break;
            case 3:
                self.scope.tmpOrder.Truck3 = value;
                break;
            case 4:
                self.scope.tmpOrder.Truck4 = value;
                break;
            case 5:
                self.scope.tmpOrder.Truck5 = value;
                break;
            case 6:
                self.scope.tmpOrder.Truck6 = value;
                break;
            case 7:
                self.scope.tmpOrder.Truck7 = value;
                break;
            case 8:
                self.scope.tmpOrder.Truck8 = value;
                break;
            case 9:
                self.scope.tmpOrder.Truck9 = value;
                break;
            case 10:
                self.scope.tmpOrder.Truck10 = value;
                break;
            case 11:
                self.scope.tmpOrder.Truck11 = value;
                break;
            case 12:
                self.scope.tmpOrder.Truck12 = value;
                break;
            case 13:
                self.scope.tmpOrder.Truck13 = value;
                break;
            case 14:
                self.scope.tmpOrder.Truck14 = value;
                break;
            case 15:
                self.scope.tmpOrder.Truck15 = value;
                break;
        }
    }

    this.getTruckData = function (order) {
        tableData = [];

        tableData.push({ Name: '1', Value: order.Truck1 | 0});
        tableData.push({ Name: '2', Value: order.Truck2 | 0});
        tableData.push({ Name: '3', Value: order.Truck3 | 0});
        tableData.push({ Name: '4', Value: order.Truck4 | 0});
        tableData.push({ Name: '5', Value: order.Truck5 | 0});
        tableData.push({ Name: '6', Value: order.Truck6 | 0});
        tableData.push({ Name: '7', Value: order.Truck7 | 0});
        tableData.push({ Name: '8', Value: order.Truck8 | 0});
        tableData.push({ Name: '9', Value: order.Truck9 | 0});
        tableData.push({ Name: '10', Value: order.Truck10 | 0});
        tableData.push({ Name: '11', Value: order.Truck11 | 0 });
        tableData.push({ Name: '12', Value: order.Truck12 | 0 });
        tableData.push({ Name: '13', Value: order.Truck13 | 0 });
        tableData.push({ Name: '14', Value: order.Truck14 | 0 });
        tableData.push({ Name: '15', Value: order.Truck15 | 0 });

        return tableData;
    }

    this.updateData = function (order) {
        jQuery(this.gridID).jqGrid('clearGridData');
        jQuery(this.gridID).jqGrid('setGridParam', { data: this.getTruckData(order) });
        jQuery(this.gridID).trigger('reloadGrid');
//        jQuery(this.gridID).jqGrid("editCell", 1, 1, true);
    }

    this.getData = function () {
        return jQuery(this.gridID).jqGrid('getGridParam', 'data');
    }

    this.stopEdit = function () {
        if (currentCell) jQuery(this.gridID).jqGrid("saveCell", currentCell.iRow, currentCell.iCol);
    }
}

function TruckTotalGrid(scope, gridID) {
    this.scope = scope;
    this.gridID = gridID;
    this.data = [];
    var self = this;
    $(this.gridID).jqGrid({
        colNames: ['Total'],
        colModel: [
            {
                name: 'Value', width: 150, editable: false, autoResizing: false, align: 'right', formatter: 'currency',
                formatoptions: { prefix: '$', suffix: '', thousandsSeparator: ',', decimalPlaces: 2, defaultValue: '' }
            }
        ],
        data: self.data,
        datatype: "local",
        autowidth: true,
        shrinkToFit: false,
        rowNum: 1,
        rownumbers: false,
        hoverrows: false,
        viewrecords: true,
        hidegrid: false,
        multiselect: false,
        cmTemplate: { title: false, resizable: false },
        beforeSelectRow: function (rowid, e) {
            return false;
        },
    });

    this.getTotalTruckAmount = function (order) {
        var totalAmount = order.Truck1 + order.Truck2 + order.Truck3 + order.Truck4 + order.Truck5 + order.Truck6 + order.Truck7 + order.Truck8 + order.Truck9 + order.Truck10 + order.Truck11 + order.Truck12 + order.Truck13 + order.Truck14 + order.Truck15;
        return totalAmount;
    }

    this.updateData = function (order) {
        var data = [{ Index: "", Name: "Total Truck Amount", Value: this.getTotalTruckAmount(order) }];
        jQuery(this.gridID).jqGrid('clearGridData');
        jQuery(this.gridID).jqGrid('setGridParam', { data: data });
        jQuery(this.gridID).trigger('reloadGrid');
    }

    this.getData = function () {
        return jQuery(this.gridID).jqGrid('getGridParam', 'data');
    }
}

