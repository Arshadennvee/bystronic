﻿function TrainingGrid(scope, gridID, onGridRowDouldClicked) {
    this.scope = scope;
    this.gridID = gridID;
    this.data = [];
    var self = this;

    var selectedRowIndex = 0;

    getColumnNames = function () {
        var columnNames = [''];

        $.each(self.scope.columns, function (index, column) {
            columnNames.push(self.scope.getColumnHeaderFor(column));
        });

        return columnNames;
    };

    getColumnFor = function (columnName) {
        switch (columnName) {
            case 'EquipmentNumber':
                return { name: 'EquipmentNumber', width: 60, editable: false, autoResizing: { minColWidth: 70 }, searchoptions: { clearSearch: true } };
            case 'JobNumber':
                return { name: 'JobNumber', align: 'center', width: 60, editable: false, autoResizing: { minColWidth: 70 }, searchoptions: { clearSearch: true } };
            case 'CustomerName':
                return { name: 'CustomerName', width: 140, editable: false, autoResizing: { minColWidth: 100 }, searchoptions: { clearSearch: true } };
            case 'CourseNumber':
                return { name: 'CourseNumber', align: 'center', width: 50, editable: false, autoResizing: { minColWidth: 40 }, searchoptions: { clearSearch: true } };
            case 'CourseName':
                return { name: 'CourseName', align: 'center', width: 100, editable: false, autoResizing: { minColWidth: 50 }, searchoptions: { clearSearch: true } };
            case 'CourseDate':
                return { name: 'CourseDate', width: 80, editable: false, autoResizing: { minColWidth: 50 }, align: 'center', sorttype: 'date', formatter: 'date', formatoptions: { newformat: 'm/d/y' }, search: false };
            case 'SeatsPurchased':
                return { name: 'SeatsPurchased', align: 'center', width: 50, editable: false, autoResizing: { minColWidth: 40 }, searchoptions: { clearSearch: true } };
            case 'SeatsUsed':
                return { name: 'SeatsUsed', align: 'center', width: 50, editable: false, autoResizing: { minColWidth: 40 }, searchoptions: { clearSearch: true } };
            case 'TraineeName':
                return { name: 'TraineeName', align: 'center', width: 90, editable: false, autoResizing: { minColWidth: 50 }, searchoptions: { clearSearch: true } };
            case 'TraineePhone':
                return { name: 'TraineePhone', align: 'center', width: 70, editable: false, autoResizing: { minColWidth: 50 }, searchoptions: { clearSearch: true } };
            case 'TraineeEmail':
                return { name: 'TraineeEmail', align: 'center', width: 90, editable: false, autoResizing: { minColWidth: 50 }, searchoptions: { clearSearch: true } };
            case 'Notes':
                return { name: 'Notes', align: 'center', width: 150, editable: false, autoResizing: { minColWidth: 50 }, searchoptions: { clearSearch: true } };
            default:
                return '';
        }
    };

    getColumns = function () {
        var columnNames =
        [
            {
                name: 'ID', width: 16, fixed: true, sortable: false, search: false, /*hidden: !self.scope.permissions.CanCreateOrder,*/ align: 'center',
                formatter: function (cellvalue, options, rowObject) {
                    var color = 'color: #e42820';
                    return "<span style='" + color + "' class='glyphicon glyphicon-remove'></span>"
                },
                cellattr: function (rowId, cellValue, rowObject) {
                    return 'title="Delete Training"';
                }
            }
        ];

        $.each(self.scope.columns, function (index, column) {
            columnNames.push(getColumnFor(column));
        });

        return columnNames;
    };

    //scope.$watch("showActiveTasksOnly", function (newValue, oldValue) {
    //    self.updateSelectedTask(scope.tasks, scope.task);
    //});

    $(this.gridID).jqGrid({
        colNames: getColumnNames(),
        colModel: getColumns(),
        data: self.data,
        datatype: "local",
        height: "auto",
        //shrinkToFit: true,
        //autowidth: false,
        //width: 900,
        width: $(document).width(),
        rowNum: 1000,
        rownumbers: true,
        hoverrows: true,
        viewrecords: true,
        caption: null,
        hidegrid: false,
        sortname: 'CourseDate',
        sortorder: "desc",
        multiselect: false,
        cmTemplate: { title: false },
        footerrow: true,
        userDataOnFooter: true,
        headertitles:true,
        sortable: {
            options: { // let reorder all columns, except with names ID, ID.
                items: ">th:not(:has(#jqgh_order_grid_cb,#jqgh_order_grid_ID,#jqgh_order_grid_rn,#jqgh_order_grid_subgrid),:hidden)"
            },
            update: function (relativeColumnOrder) {
                var columnNames = $(self.gridID).jqGrid("getGridParam", "colNames");
                var columns = [];
                $.each(columnNames, function (index, columnName) {
                    if (columnName)
                        columns.push(self.scope.getColumnNameFor(columnName));
                });
                self.scope.columns = columns;
                self.scope.saveColumnsAfterDragAndDrop();
            }
        },
        loadComplete: function () {
            var rowIds = $(this).jqGrid('getDataIDs');
            //if (rowIds.length === 0) return;
            if (rowIds.length > selectedRowIndex) 
                $(this).setSelection(rowIds[selectedRowIndex]);
        },
        ondblClickRow: function (rowId) {
            $(this).setSelection(rowId);
            var course = $(this).jqGrid("getLocalRow", rowId);
            onGridRowDouldClicked(course.Id);
        },
        beforeSelectRow: function (rowId, e) {
            var idArray = $(this).jqGrid('getDataIDs');
            var selectedRowID = $(this).jqGrid("getGridParam", "selrow");
            var iCol = $.jgrid.getCellIndex($(e.target).closest("td")[0]);

            var course = $(this).jqGrid("getLocalRow", rowId);
            if (iCol == 1) {
                showYesNoMessage('Training', 'Do you want to delete Training ' + course.CourseName  + '?',
                    function () {
                        self.scope.deleteTraining(course);
                    }
                );
            }

            if (selectedRowID == rowId) {
                return false;
            }

            selectedRowID = rowId;
            selectedRowIndex = _.findIndex(idArray, function (id) { return id == selectedRowID; });
            return true;
        }
    });

    jQuery(this.gridID).jqGrid('filterToolbar', { searchOnEnter: false, stringResult: true, defaultSearch: 'cn' });

    this.filterData = function (data, filter) {
        var filteredData = [];

        $.each(data, function (index, course) {
            var order = _.find(self.scope.orders, function (o) { return o.Id == course.OrderId; });
            var customer = _.find(self.scope.customers, function (c) { return c.Id == order.CustomerId; });

            var _course = jQuery.extend(true, {}, course); // deep copy
            _course.CustomerName = customer.Name;
            if (self.matchCourse(_course, filter)) {
                filteredData.push(_course);
            }
        });

        return {
            tableData: filteredData 
        };
    }

    this.matchCourse = function (course, filter) {
        var matches = true;
        return matches;
    }

    this.updateData = function (courses, filter) {
        var data = this.filterData(courses, filter);
        jQuery(this.gridID).jqGrid('clearGridData');
        jQuery(this.gridID).jqGrid('setGridParam', {
            data: data.tableData//,
            //userData: {
            //    'Customer': 'Total', 'TotalListPrice': this.totalListPrice, 'SalePrice': this.totalSellingPrice, 'DiscountAmount': this.totalDiscount,
            //    'DealerCommissionInfo': this.formatCurrency(this.totalDealerCommission), 'RegionalManagerCommission': this.totalRegionalManagerCommission, 'ProductManagerCommission': this.totalProductManagerCommission
            //}
        });
        jQuery(this.gridID).trigger('reloadGrid');
    }

    this.getData = function () {
        return jQuery(this.gridID).jqGrid('getGridParam', 'data');
    }

    this.export = function (format) {
//        var data = $(gridID).jqGrid('getRowData');
//        var totalData = $(gridID).jqGrid('footerData', 'get');
//        var columnNames = $(gridID).jqGrid('getGridParam', 'colNames');
//        $.each(data, function (index, row) {
//            delete row['ID'];
//        });
//        if (format == 'csv')
//            CSVExport(columnNames, data, (self.scope.IsDemoVersion ? '' : 'Bystronic') + 'Orders.csv');
//        else
//            orderTableToPDF(self.scope, columnNames, data, totalData, (self.scope.IsDemoVersion ? '' : 'Bystronic') + 'Orders.pdf');
    }
}
